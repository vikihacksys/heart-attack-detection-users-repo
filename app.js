//Users Microservices

const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const app = express();

app.use(morgan('dev'))
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())
 
mongoose.connect("mongodb+srv://vikihacksys:Vivek.Radadiya@123@cluster0-ujj5m.mongodb.net/UserDB?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
}) 

// app.use((req, res, next) => {
// 	res.header("Access-Control-Allow-Origin", "*")
// 	res.header(
// 		"Access-Control-Allow-Headers",
// 		"Origin, X-Requested-With, Content-Type, Accept, Authorization"
// 	)
// 	console.log("outside " + req.method) 
// 	if (req.method === "GET" || req.method === "POST" || req.method === "PUT" || req.method === "PATC" || req.method === "DELETE") {
// 		console.log("inside")
// 		res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE')
// 		// return res.status(200).json({})
// 	}
// })

const registerRoutes = require('./api/routes/register')
const loginRoutes = require('./api/routes/login')
const forgotPasswordRoutes = require('./api/routes/forgotPassword')
const fetchUsersRoutes = require('./api/routes/fetchUsers')
const userProfileRoutes = require('./api/routes/userProfile')
const fetchAllPatientsRoutes = require('./api/routes/fetchAllPatients')

app.use('/register', registerRoutes)
app.use('/login', loginRoutes)
app.use('/forgotpassword', forgotPasswordRoutes)
app.use('/fetchUsers', fetchUsersRoutes)
app.use('/profile', userProfileRoutes)
app.use('/fetchAllPatients', fetchAllPatientsRoutes)

app.use((req, res, next) => {
	const error = new Error('Not found')
	error.status = 404
	next(error)
})

app.use((error, req, res, next) => {
	res.status(error.status || 500)
	res.json({
		error: {
			message: error.message
		}
	})
})
 
module.exports = app;