const mongoose = require('mongoose')

const userSchema = mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
	firstname: { type: String, required: true},
	lastname: { type: String, required: true},
	mobile_number: { type: String, required: true},
	email_id: { type: String, required: true},
	password: { type: String, required: true}, 
	age: { type: Number, required: true},
	gender: { type: String, required: true},
	date_of_birth: { type: Date, required: true},
	address: { type: String, required: true},
	user_type: { type: String, required: true},
	is_registered: { type: Boolean, required: false} 
})

module.exports = mongoose.model('User', userSchema)