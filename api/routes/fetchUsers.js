const express = require('express');
const router = express.Router();
const mongoose = require('mongoose')

const User = require('../models/userModel')


/* 

    API: "http://localhost:8080/fetchUsers"

    Parameters:
        doctor_mobile_number: +491325465651654
        patients: [+4913254651635, +4912354684623, +49132546846512]

    HTTP-method: POST   

*/

router.post('/', (req, res, next) => {

    User.find({'mobile_number': {$in: req.body.patients}})
    	.exec()
    	.then(result => {

            //console.log("patients list: " + result)

    		if (result.length > 0) {

    			res.status(200).json({
                    status: true,
                    message: "Patients successfully fetched.", 
                    data: result
                })

    		} else {
    			res.status(200).json({
                    status: false,
                    message: "No patients found.", 
                    data: {} 
                })
    		}
    		
    	})
    	.catch(error => {
    		sendErrorResponse (res, error)	
    	})
 
})

function sendErrorResponse (res, error) {
    res.status(500).json({
        status: false,
        message: "",
        data: error
    })
}

module.exports = router