//login.js

const express = require('express');
const router = express.Router();
const mongoose = require('mongoose')
var nodemailer = require('nodemailer');

const User = require('../models/userModel')

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'imjordan1269@gmail.com',
    pass: 'vivek@12345'
  }
});


/* 

    API: "http://localhost:8080/forgotpassword"

    Parameters:
        email_id: "amitundhad111@gmail.com" 

    HTTP-method: POST   

*/

router.post('/', (req, res, next) => {

    User.find({'email_id': req.body.email_id})
    	.exec()
    	.then(result => { 

    		if (result.length > 0) {
                console.log("result : " + result) 

                // var randomPassword = Math.random().toString(36).slice(2)
                var randomPassword = generateRandomPassword()
                var emailText = "Please login using this temporary password : " + randomPassword + "\n" + "After login, you can change your password from your profile page."
                console.log("emailtext : " + emailText)

                var mailOptions = {
                    from: 'imjordan1269@gmail.com',
                    to: req.body.email_id,
                    subject: 'Password recovery for your Heart Attack Detection app.',
                    text: emailText
                };

                transporter.sendMail(mailOptions, function(error, info) {

                    if (error) {

                        console.log("Error sending email : " + error);
                        res.status(200).json({ 
                            status: false,
                            message: "Error sending password recovery email.",
                            data: {}
                        })

                    } else {

                        console.log('Email sent: ' + info.response);
 
                        User.updateOne({'email_id' : req.body.email_id}, {$set: {
                            'password' : randomPassword
                        }}).exec()
                        .then(result => {
                            console.log("updated : " + result)
                            res.status(200).json({ 
                                status: true,
                                message: "Recovery email sent successfully.",
                                data: info.response
                            })
                        })
                        .catch(error => {
                            console.log("updated error : " + error)
                            res.status(200).json({ 
                                status: false,
                                message: "Error updating password.",
                                data: {}
                            })   
                        })
 
                    }

                }); 

    		} else {

    			res.status(200).json({ 
                    status: false,
                    message: "This email address is not registered.",
                    data: {}
                })

    		}
    		
    	})
    	.catch(error => {
            console.log("Error = " + error)
    		res.status(500).json({ 
				error: error
			})	
    	})
 
})



function generateRandomPassword() {

    var randomPassword = ""
    var capitalLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    var smallLetters = "abcdefghijklmnopqrstuvwxyz"
    var digits = "0123456789"
    var specialChars = "@#$&."

    randomPassword += capitalLetters.charAt(Math.floor(Math.random() * capitalLetters.length));
    randomPassword += digits.charAt(Math.floor(Math.random() * digits.length));
    randomPassword += smallLetters.charAt(Math.floor(Math.random() * smallLetters.length));
    randomPassword += specialChars.charAt(Math.floor(Math.random() * specialChars.length));
    randomPassword += digits.charAt(Math.floor(Math.random() * digits.length));
    randomPassword += capitalLetters.charAt(Math.floor(Math.random() * capitalLetters.length));
    randomPassword += smallLetters.charAt(Math.floor(Math.random() * smallLetters.length));
    randomPassword += capitalLetters.charAt(Math.floor(Math.random() * capitalLetters.length));
    randomPassword += specialChars.charAt(Math.floor(Math.random() * specialChars.length));
    randomPassword += digits.charAt(Math.floor(Math.random() * digits.length));
 
    return randomPassword

}

module.exports = router