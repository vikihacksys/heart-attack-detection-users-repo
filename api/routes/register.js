const express = require('express');
const router = express.Router();
const mongoose = require('mongoose')
const request = require("request");
  
const User = require('../models/userModel')


/* 

    API: "http://localhost:8080/register"

    Parameters:
        firstname:Josh
		lastname:Klein
		mobile_number:+491543116252324 
		age:25
		gender: Male/Female
		address:Kirchehof 10, 43553 Germany
		user_type:Doctor/Patient 
		date_of_birth:2020/05/15
		email_id:amitundhad111@gmail.com
		password:josh12345 

    HTTP-method: POST   

*/

router.post('/', (req, res, next) => {
 
	User.find({'email_id': req.body.email_id })
    	.exec()
    	.then(result => {

    		if (result.length > 0) {
    			res.status(200).json({ 
    				status: false,
    				message: "This email address is already registered.",
    				data: {}
    		    })
    		} else {

    			User.find({ 'mobile_number': req.body.mobile_number })
    				.exec()
    				.then(data => {

    					if (data.length > 0) {
    						res.status(200).json({ 
			    				status: false,
			    				message: "This mobile number is already registered.",
			    				data: {}
			    		    })
    					} else {

    						let user = new User ({
								_id: new mongoose.Types.ObjectId(),
								firstname: req.body.firstname,
								lastname: req.body.lastname,
								mobile_number: req.body.mobile_number, 
								email_id: req.body.email_id,
								password: req.body.password,
								address: req.body.address,
								age: req.body.age,
								gender: req.body.gender,
								date_of_birth: new Date(req.body.date_of_birth), 
								user_type: req.body.user_type,
								is_registered: true   
							}) 

							user.save()
							.then(result => { 
			 
								console.log("Register API response : " + user) 
			 
								res.status(200).json({
									status: true,
									message: "Registered successfully.", 
									data: user
								})

							 	//callSendOTPService(req, res, user)
							 	  
							})
							.catch(error => {
								sendErrorResponse(res, error)
							})

    					}

    				})
    				.catch(error => {
			    		sendErrorResponse(res, error)	
			    	})
    
    		}
    		
    	})
    	.catch(error => {
    		sendErrorResponse(res, error)	
    	})

})

function sendErrorResponse (res, error) {
	res.status(500).json({
		status: false,
		message: "",
		data: error
    })
}

function callSendOTPService (req, res, user) {
 
	request.post({

	    "headers": { "content-type": "application/json" },
	    "url": "http://localhost:8081/sendOTP",
	    "body": JSON.stringify({
	        "mobile_number": req.body.mobile_number 
	    })

	}, (error, response, body) => { 

		let parsedData = JSON.parse(body) 	
		
	    if(error) {

	        res.status(500).json({
				status: false,
				message: "",  
				data: error
			})

	    } else {

	    	if (parsedData.status) { 

				res.status(200).json({
					status: true,
					message: "Registered successfully.", 
					data: user
				})

			} else { 	

				res.status(200).json({
					status: false,
					message: "Failed to send OTP.", 
					data: body.data
				})

			}

	    } 

	});

} 

module.exports = router