//login.js

const express = require('express');
const router = express.Router();
const mongoose = require('mongoose')

const User = require('../models/userModel')


/* 

    API: "http://localhost:8080/login"

    Parameters:
        email_id: "amitundhad111@gmail.com"
        password: "2s7pu6fe0a5" 

    HTTP-method: POST   

*/

router.post('/', (req, res, next) => {

    User.find({'email_id': req.body.email_id, 'password': req.body.password})
    	.exec()
    	.then(result => {

    		if (result.length > 0) {

    			res.status(200).json({ 
                    status: true,
                    message: "Logged-in successfully.",
                    data: result[0]
                })

    		} else {
    			res.status(200).json({ 
                    status: false,
                    message: "Wrong email address or password.",
                    data: {}
                })
    		}
    		
    	})
    	.catch(error => {
    		res.status(500).json({
				error: error
			})	
    	})
 
})

module.exports = router