const express = require('express');
const router = express.Router();
const mongoose = require('mongoose')

const User = require('../models/userModel')


/* 

    API: "http://localhost:8080/fetchAllPatients"

    Parameters: Not required

    HTTP-method: GET   

*/

router.get('/', (req, res, next) => {

    User.find({'user_type': 'Patient'})
    	.exec()
    	.then(result => {

            console.log("patients list: " + result)

    		if (result.length > 0) {

    			res.status(200).json({
                    status: true,
                    message: "Patients successfully fetched.", 
                    data: result
                })

    		} else {
    			res.status(200).json({
                    status: false,
                    message: "No patients found.", 
                    data: {} 
                })
    		}
    		
    	})
    	.catch(error => {
    		sendErrorResponse (res, error)	
    	})
 
})

function sendErrorResponse (res, error) {
    res.status(500).json({
        status: false,
        message: "",
        data: error
    })
}

module.exports = router