const express = require('express');
const router = express.Router();
const mongoose = require('mongoose')
const request = require("request");

const User = require('../models/userModel')


/* 

    API: "http://localhost:8080/profile"

    Parameters:
        email_id: "amitundhad111@gmail.com"

    HTTP-method: POST   

*/

router.post('/', (req, res, next) => {

    User.find({'email_id': req.body.email_id})
    	.exec()
    	.then(result => {

    		if (result.length > 0) {

    			res.status(200).json({ 
                    status: true,
                    message: "",
                    data: result[0]
                })

    		} else {
    			res.status(200).json({ 
                    status: false,
                    message: "No user found.",
                    data: {}
                })
    		}
    		
    	})
    	.catch(error => {
    		res.status(500).json({
				error: error
			})	
    	})
 
})


/* 

    API: "http://localhost:8080/profile/editProfile"

    Parameters:
        firstname:Josh
        lastname:Klein
        mobile_number:+491543116252324 
        age:25
        gender: Male/Female
        address:Kirchehof 10, 43553 Germany
        user_type:Doctor/Patient 
        date_of_birth:2020/05/15
        email_id:amitundhad111@gmail.com
        password:josh12345
         
    HTTP-method: POST   

*/

router.post('/editProfile', (req, res, next) => {

    let user = new User ({
        _id: req.body._id,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        mobile_number: req.body.mobile_number, 
        email_id: req.body.email_id,
        password: req.body.password,
        address: req.body.address,
        age: req.body.age,
        date_of_birth: new Date(req.body.date_of_birth), 
        user_type: req.body.user_type,
        is_registered: true   
    })

    User.findByIdAndUpdate(req.body._id, user, {new: true}, function(err, data) {

        if (err) {
             
            res.status(200).json({ 
                status: false,
                message: "Profile not updated.",
                data: err
            })

        } else { 
            res.status(200).json({ 
                status: true,
                message: "Profile updated successfully.",
                data: data
            })
        }

    })
 
})


/* 

    API: "http://localhost:8080/profile/deleteProfile"

    Parameters:
        email_id: "amitundhad111@gmail.com"

    HTTP-method: POST   

*/

router.post('/deleteProfile', (req, res, next) => {

    User.find({'email_id': req.body.email_id})
        .exec()
        .then(result => {

            if (result.length > 0) {
                //console.log("result : ", result[0])
                //console.log("user : ", result[0].user_type)

                request.post({

                    "headers": { "content-type": "application/json" },
                    "url": "http://ec2-18-217-9-117.us-east-2.compute.amazonaws.com:8082/patientsList/deletePatient",
                    "body": JSON.stringify({
                        "user_mobile_number": result[0].mobile_number,
                        "user_type": result[0].user_type  
                    })

                }, (error, response, body) => { 

                    //console.log("body : ", body)
                    let parsedData = JSON.parse(body)
                    console.log("parsedData " + parsedData)   
                    
                    if(error) {

                        res.status(500).json({
                            status: false,
                            message: "",  
                            data: error
                        })

                    } else {

                        if (parsedData.status) { 

                            User.findOneAndDelete({'email_id': req.body.email_id})
                            .then(deletedDocument => {

                                if (deletedDocument) {

                                    res.status(200).json({ 
                                        status: true,
                                        message: "Account deleted successfully.",
                                        data: deletedDocument
                                    })

                                } else { 

                                    res.status(200).json({ 
                                        status: false,
                                        message: "Error while deleting account.",
                                        data: {}
                                    })

                                }

                            })
 
                        } else {    

                            res.status(200).json({
                                status: false,
                                message: "Error deleting data of user.", 
                                data: {} 
                            })

                        }

                    } 

                });
 
            } else {
                res.status(200).json({ 
                    status: false,
                    message: "No user found.",
                    data: {}
                })
            }
            
        })
        .catch(error => {
            res.status(500).json({
                error: error
            })  
        })
 
})


module.exports = router